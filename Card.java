public class Card{
	
	private int value;
	private String suit;
	
	public Card(int value, String suit){
		this.value = value;
		this.suit = suit;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		if(value == 1){
			return "Ace of " + suit + "\n";
		}
		else if(value == 11){
			return "Jack of " + suit + "\n";
		}
		else if(value == 12){
			return "Queen of " + suit + "\n";
		}
		else if(value == 13){
			return "King of " + suit + "\n";
		}
		else{
			return value + " of " + suit + "\n";
		}
	}
	
	public double calculateScore(){
		double score = value; 
		
		if(suit.equals("Hearts")){
			score += 0.4;
		}
		else if(suit.equals("Spades")){
			score += 0.3;
		}
		else if(suit.equals("Diamonds")){
			score += 0.2;
		}
		else if(suit.equals("Heart")){
			score += 0.1;
		}	
		return score;
	}
}