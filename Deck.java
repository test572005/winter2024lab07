import java.util.Random;

public class Deck{
	
	int numberOfCards;
	Card[] cards;
	Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards];
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		int[] value = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
		
		int i = 0;
		for(/*int v = 1; v <= 14; v++*/ int v: value){
			for(String s: suits){
				if(i != 52){
					cards[i] = new Card(v, s);
					i++;
				}
			}
		}
	}
		
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		if(this.numberOfCards > 0){
			Card topCard = this.cards[this.numberOfCards - 1];
			this.numberOfCards --;
			return topCard;
		}
		return null;
	}
		
	public String toString(){
		String s = "Currently has" + numberOfCards + "cards. \n";
			for(int i = 0; i < cards.length; i++){
				s += cards[i].toString();
			}
			return s;
			}
	
		public void shuffle(){
			for(int i = 0; i < this.numberOfCards; i++){
				int index = i + rng.nextInt(this.numberOfCards - i);
				Card tempo = this.cards[i];
				this.cards[i] = this.cards[index];
				this.cards[index] = tempo;
			}
		}
}