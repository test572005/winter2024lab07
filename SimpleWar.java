public class SimpleWar{
	
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();

		int playerOnePoint = 0;
		int playerTwoPoint = 0;
		
		while(deck.numberOfCards > 0){
		
			System.out.println("Draw your cards!");
			
			Card playerCardOne = deck.drawTopCard();
			Card playerCardTwo = deck.drawTopCard();
		
			System.out.println("Player 1 card: " + playerCardOne);
			System.out.println("Player 2 card: " + playerCardTwo);
		
			double cardOneScore = playerCardOne.calculateScore();
			double cardTwoScore = playerCardTwo.calculateScore();
		
			System.out.println("Player 1 intitial points: " + playerOnePoint);
			System.out.println("Player 2 intitial points: " + playerTwoPoint);
		
			System.out.println("Player 1 card score: " + cardOneScore);
			System.out.println("Player 2 card score: " + cardTwoScore);
		
			if(cardOneScore > cardTwoScore){
				System.out.println("Player one wins!");
				playerOnePoint++;
			}
			else if(cardTwoScore > cardOneScore){
				System.out.println("Player two wins!");
				playerTwoPoint++;
			}
			else{
				System.out.println("It's a tie");
			}
		
			System.out.println("Player 1 updated points: " + playerOnePoint);
			System.out.println("Player 2 updated points: " + playerTwoPoint + "\n");
		}
			
		if(playerOnePoint > playerTwoPoint){
			System.out.println("Congratulations! Player one wins this round!");
		}
		else if(playerTwoPoint > playerOnePoint){
			System.out.println("Congratulations! Player two wins this round!");
		}
	}
	
}